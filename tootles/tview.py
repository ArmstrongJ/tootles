
class TweetsList():
    def __init__(self, listing, db):
        
        self.db = db
        self.listing = listing
        
        self.listing.tag_configure('odd', background='white smoke')
        
        self.refresh()
        
    def clear(self):
        items = self.listing.get_children()
        if len(items) > 0:
            self.listing.delete(*items)
            
        self.listing.selection(items=[])
        #self.listing.event_generate("<<TreeviewSelect>>")
        
    def refresh(self):
        self.clear()
        i = 0 
        for tweet in self.db.all():
            
            tags = []
            if i % 2 != 0:
                tags.append('odd')

            self.listing.insert('', 
                                'end', 
                                text="{0} (@{1})".format(tweet['name'],
                                                         tweet['screen_name']),
                                iid=tweet['_id'],
                                tags=tags)
                                
            self.listing.set(tweet['_id'], column='User', 
                             value="{0} (@{1})".format(tweet['name'],
                                                       tweet['screen_name']))
            self.listing.set(tweet['_id'], column='Tweet', value=tweet['text'])
            self.listing.set(tweet['_id'], column='Time', value=tweet['created_at'])
            self.listing.set(tweet['_id'], column='Details', value='')
            
            i = i + 1

            
    