from twitter import *
from config import read_auth

def translate(tweet):
    return {'name':tweet['user']['name'], 
            'screen_name':tweet['user']['screen_name'],
            'text':tweet['text'],
            'created_at':tweet['created_at']}

def refresh_from_Twitter(db):
    creds = read_auth()
    
    twitter = Twitter(auth=OAuth(creds['user_key'],
                                 creds['user_secret'],
                                 creds['api_key'],
                                 creds['api_secret']))
                                 
    for tweet in twitter.statuses.home_timeline(count=20):
        db.insert(translate(tweet))

    