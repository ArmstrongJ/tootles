import os.path
import json

def config_directory():
    return os.path.expanduser(os.path.join("~",".tootles"))

def read_auth():
    config = {}
    filename = os.path.join(config_directory(), "auth.json")
    with open(filename, "r") as fp:
        config = json.load(fp)
        
    return config
