from tkinter import *
from tkinter.ttk import *

import db
from tview import TweetsList
from tcom import refresh_from_Twitter

class Tootles():
    def __init__(self, root, tdb):
        
        self.db = tdb
        
        self.frame = Frame(root)
        
        sframe, self.stream = self.buildstream(self.frame)
        sframe.grid(row=0, column=1, sticky=(N, S, E, W), padx=5, pady=5)
        
        buttons = self.buildbuttons(self.frame)
        buttons.grid(row=0, column=0, sticky=(N, S, E, W), pady=5, padx=3)
        
        self.frame.columnconfigure(1, weight=2)
        self.frame.rowconfigure(0, weight=2)
        
        self.frame.pack(expand=True, fill=BOTH)
        
        self.listcontroller = TweetsList(self.stream, self.db)
    
    def buildstream(self, master):
        sframe = Frame(master)
        
        columns = ('User', 'Tweet', 'Time', 'Details')
        listing = Treeview(sframe, columns=columns, show="headings")
        for x in columns:
            listing.heading(x, text=x, anchor=(W,))
        listing.grid(row=0, column=0, sticky=(N,S,E,W))
        
        scr = Scrollbar(sframe, orient=VERTICAL, command=listing.yview)
        listing.configure(yscrollcommand=scr.set)
        scr.grid(row=0, column=1, sticky=(N,S,W))
        
        sframe.rowconfigure(0, weight=2)
        sframe.columnconfigure(0, weight=2)
        
        return sframe, listing
    
    def buildbuttons(self, master):
        bframe = Frame(master)
        
        login = Button(bframe, text="Login", command=self.login)
        login.grid(row=0, column=0, sticky=(N, S, E, W), pady=2)
        
        refresh = Button(bframe, text="Refresh", command=self.refresh)
        refresh.grid(row=1, column=0, sticky=(N, S, E, W), pady=2)
        
        quitbutton = Button(bframe, text="Quit", command=self.quit)
        quitbutton.grid(row=2, column=0, sticky=(N, S, E, W), pady=2)
        
        bframe.columnconfigure(0, weight=2)
        
        return bframe
    
    def login(self):
        pass
        
    def refresh(self):
        refresh_from_Twitter(self.db)
        
        self.update_stream_display()
        
    def update_stream_display(self):
        self.listcontroller.refresh()
    
    def quit(self):
        self.frame.quit()


def startup():
    tdb = db.TweetsDatabase()
    
    root = Tk()
    
    root.title('Tootles')

    app = Tootles(root, tdb)
    root.mainloop()
    

if __name__ == '__main__':
    startup()
    