import sostore
import datetime

class TweetsDatabase(sostore.Collection):
    def __init__(self):
        sostore.Collection.__init__(self, "tweets")
        
        # Some fakes
        one = {"screen_name":"@mflower", 
               "name":"Margaux LaFleur",
               "text":"New pair of shoes today!",
               "created_at":datetime.datetime.now().isoformat()}
               
        two = {"screen_name":"@mdoublec", 
               "name":"Henry McCallum",
               "text":"Enough with asking me to do things for you!",
               "created_at":datetime.datetime.now().isoformat()}
               
        self.insert(one)
        self.insert(two)
        